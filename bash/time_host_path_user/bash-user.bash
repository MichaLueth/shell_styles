# Bash prompts colors
# Sometimes the `\H` variable does not return the full hostname; so we can define HFN=$(hostname -f) and replace `\H` by $HFN.
PS1="\n[\t] \H: \[\e[1m\]\w\[\e[m\]\n\u \[\e[1m\]$\[\e[m\] "
