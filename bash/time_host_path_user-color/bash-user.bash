# Bash prompts colors
# Sometimes the `\H` variable does not return the full hostname; so we can define HFN=$(hostname -f) and replace `\H` by $HFN.
PS1="\n[\t] \[\e[0;36m\]\H\[\e[m\]: \[\e[1;33m\]\w\[\e[m\]\n\[\e[0;32m\]\u\[\e[m\] \[\e[1;32m\]$\[\e[m\] "
